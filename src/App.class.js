import React, { Component } from 'react';
import uuid from "uuid";

import Threads from "./components/Threads"; //gets the threads.js
import ThreadForm from "./components/ThreadForm"; //gets the ThreadForm.js

class App extends Component {
// const App = () => {

  // state = { //state is a predefined word
  //  threads:[
  //  {
  //    "userId": 1,
  //    "id": 1,
  //    "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
  //    "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
  //  },
  //  {
  //    "userId": 1,
  //    "id": 2,
  //    "title": "qui est esse",
  //    "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
  //  },
  //  {
  //    "userId": 1,
  //    "id": 3,
  //    "title": "ea molestias quasi exercitationem repellat qui ipsa sit aut",
  //    "body": "et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut"
  //  },
  //  {
  //    "userId": 1,
  //    "id": 4,
  //    "title": "eum et est occaecati",
  //    "body": "ullam et saepe reiciendis voluptatem adipisci\nsit amet autem assumenda provident rerum culpa\nquis hic commodi nesciunt rem tenetur doloremque ipsam iure\nquis sunt voluptatem rerum illo velit"
  //  }
  //  ],
  //  "showForm":false
  // }
  state = {
    threads : [],
    showForm: false,
    isLoading: true
  }

  // const [isLoading, setIsLoading] = useState(true);
  // const [threads, setThreads] = useState([]);
  // const [showForm, setShowForm] = useState(false);

  //component lifecycle -> events
  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/posts")
    .then(res => res.json())
    .then(threads => {
      this.setState({
        threads: threads,
        isLoading: false
      })
    })
  }

  style = {
    "width" : "50%",
    "margin": "0 auto"
  }

  threadFormSubmit = ({title, body}) => {
    // console.log(thread)
    const newThread = {
      "userId":1,
      // "id": this.state.threads.length + 1,
      "id": uuid.v4(),
      "title": title,
      "body": body
    }

    this.setState({
      threads: [
        ...this.state.threads,
        newThread
      ]
    });
  }


  createThreadClickHandler = () => {
    this.setState({
      showForm:true
    })
  }

  threadFormCancel = () => {
    this.setState({
      showForm:false
    })
  }

  deleteThread = (threadId) => {
    // console.log("post to delete: "+ threadId.threadId);
    let newThreads = this.state.threads.filter(thread => thread.id !== threadId );
    // console.log(newThreads);
    this.setState({
      threads: newThreads
    });
  }


  editThread = (threadId, title, body) => {
    // console.log(threadId);
    // console.log(body);
    // console.log(title);
    let editedThreads = this.state.threads.map(thread => {
      if(thread.id === threadId){
        thread.title = title;
        thread.body = body;
        return thread;
      } else {
        return thread;
      }
    });
    // console.log(editedThreads);
    this.setState({
      threads: editedThreads
    })
  }

  render(){
    const threadForm = this.state.showForm ? 
      <ThreadForm submit= {this.threadFormSubmit} cancel = {this.threadFormCancel}/> : 
      <button onClick={this.createThreadClickHandler}>Create New Thread</button>

    const threads = this.state.threads.length === 0 ? <h2>No posts</h2> : 
    <Threads threads={this.state.threads} deleteThread = {this.deleteThread} editThread = {this.editThread}/> 

    const loading = this.state.isLoading ? "Loading data" : threads

    return (
      <div style = {this.style} className="App">
        {threadForm}
        {loading}
      </div>
    );
  }
}

export default App;
