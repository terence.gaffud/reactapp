import React, {Component} from "react";

// const ThreadForm = () => {
// 	return(
// 		<form>
// 			<label htmlfor="title">Title</label>
// 			<input type="text" id="title" />
// 			<br />
// 			<label htmlfor="body">Body</label>
// 			<textarea id="body" cols="20" rows="5" />
// 			<button>Submit</button>
// 		</form>
// 	);
// }

class ThreadForm extends Component {
	state = {
		// "title" : "",
		// "body": "",
		"title" : this.props.title ? this.props.title : "",
		"body" : this.props.body ? this.props.body : "",
		// posts: [
		// 	id1: 1,
		// 	id2: 2,
		// 	id3: 3
		// ]
	}

	// this.setState({
	// 	posts:[...this.state.posts, id3:5];
	// })

	titleChangeHandler = (e) => { //gets the value
		this.setState({"title": e.target.value});
	}

	bodyChangeHandler = (e) => {
		this.setState({"body": e.target.value});
	}

	submitClickHandler = (e) => {
		e.preventDefault();
		this.props.submit(this.state);
	}

	cancelClickHandler = (e) => {
		e.preventDefault();
		this.props.cancel();
	}

	render(){
		// console.log(this.state);
		const cancelButton = this.props.cancel ? 
		<button type="button" onClick={this.props.cancel}>Cancel</button> : 
		<button type="button" onClick={this.cancelClickHandler} >Cancel</button>

		const submitButton = this.props.editsubmit ? 
		<button type="button" onClick={()=>this.props.editsubmit(this.state)}>Submit</button> :
		<button type="button" onClick={this.submitClickHandler}>Submit</button>

		return(
			<form>
				<label htmlFor="title">Title</label>
				<input type="text" id="title" onChange={this.titleChangeHandler} value={this.state.title}/>
				<br />
				<label htmlFor="body">Body</label>
				<textarea id="body" cols="20" rows="5" onChange={this.bodyChangeHandler} value={this.state.body}/>
				<br />
				{/*<button type="button" onClick={this.submitClickHandler}>Submit</button>*/}
				{submitButton}
				{/*<button type="button" onClick={this.cancelClickHandler}>Cancel</button>*/}
				{cancelButton}
			</form>
		)
	}
}

export default ThreadForm;