// import React from 'react';
import React, { Component } from 'react';
// { Component }

// const Threads = () => { //same name as the file
// 	return(
// 		<div>
// 			<p>Hello World</p>
// 		</div>
// 	);
// }

import Thread from "./Thread";

class Threads extends Component {
	// state = { //state is a predefined word
	// 	threads:[
	// 	{
	// 		"userId": 1,
	// 		"id": 1,
	// 		"title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
	// 		"body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
	// 	},
	// 	{
	// 		"userId": 1,
	// 		"id": 2,
	// 		"title": "qui est esse",
	// 		"body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
	// 	},
	// 	{
	// 		"userId": 1,
	// 		"id": 3,
	// 		"title": "ea molestias quasi exercitationem repellat qui ipsa sit aut",
	// 		"body": "et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut"
	// 	},
	// 	{
	// 		"userId": 1,
	// 		"id": 4,
	// 		"title": "eum et est occaecati",
	// 		"body": "ullam et saepe reiciendis voluptatem adipisci\nsit amet autem assumenda provident rerum culpa\nquis hic commodi nesciunt rem tenetur doloremque ipsam iure\nquis sunt voluptatem rerum illo velit"
	// 	}
	// 	]
 //  }

  // style = {
  // 	"width": "50%",
  // 	"margin": "0 auto"
  // }

 // 	threads = this.state.threads.map( (thread) => {
 //  	return(
 //  		<div>
	// 			<h2>{thread.title}</h2>
	// 			<p>{thread.body}</p>
	// 			<p>Written by: {thread.userId}</p>
	// 		</div>
 //  	)
 //  });

	// render(){
	// 	return(
	// 		<div>
	// 			{this.threads}
	// 		</div>
	// 	);
	// }

	//or 
	render(){
		const threads = this.props.threads.map( (thread) => {
			return(
				<Thread key={thread.id} title={thread.title} body={thread.body} userId={thread.userId} threadId={thread.id} deleteThread = {this.props.deleteThread} editThread={this.props.editThread} addComment = {this.props.addComment} comments = {thread.comments}/>
			)
		}).reverse();

		return(
			// <div style = {this.style}>
			<div>
				{threads}
			</div>
		);
	}
}


//we need to convert the functional component to class based

export default Threads;

