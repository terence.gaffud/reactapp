import React, { useState } from "react";

const CommentForm = ({threadId, addComment}) => {
	const [commentBody, setCommentBody] = useState("");

	const inputChangeHandler = (e) => {
		setCommentBody(e.target.value);
	}

	const submitClickHandler = () => {
		addComment(threadId, commentBody);
	}

	return(
		<form>
			<textarea onChange={inputChangeHandler} value={commentBody} /><br/>
			<button type="button" onClick={submitClickHandler}>Comment</button>
		</form>
	)
}

export default CommentForm
