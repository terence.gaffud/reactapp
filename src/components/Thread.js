import React, { useState } from "react"; //you're able to use States without converting to class
import ThreadForm from "./ThreadForm";
import CommentForm from "./CommentForm";
import Comments from "./Comments";

// import PropTypes from "prop-types";

// const Thread = (props) => {
// 	return (
// 		<div>
// 			<h2>{props.title}</h2>
// 			<p>{props.body}</p>
// 			<p>Written by: {props.userId}</p>
// 		</div>
// 	);
// }


const Thread = ({title, body, userId, threadId, deleteThread, editThread, addComment, comments}) => { //destructured
	const style = {
		"backgroundColor":"lightgrey",
		"border":"1px solid black",
		"padding":"10px",
		"margin":"25px"
	}

	// const deleteButtonClickHandler = (e) => {
	// 	// threadId = e.target.getAttribute("id");
	// 	// console.log(threadId);
	// 	// console.log({threadId})
	// 	deleteThread(threadId);	
	// }

	const [state, setState] = useState({ editing: false });
	//use state returns an array so to get the state and set the state (like destructuring for arrays)

	const editClickHandler = () => {
		setState({
			editing: true
		})
	}

	const cancelClickHandler = () => {
		setState({
			editing:false
		})
	}

	const editSubmitClickHandler = ({title, body}) => {
		editThread(threadId, title, body);
		setState({
			editing:false
		})
	}

	const thread = state.editing ? 
		<ThreadForm cancel={cancelClickHandler} editsubmit={editSubmitClickHandler} title={title} body={body}/> : 
		<div style={style}>
			<h2>{title}</h2>
			<p>{body}</p>
			<p>Written by: {userId}</p>
			{/*<button onClick={deleteButtonClickHandler}>Delete post</button>*/}
			<button onClick={editClickHandler}>Edit</button>
			<button onClick={() => deleteThread(threadId)}>Delete post</button>
			<CommentForm threadId = {threadId} addComment={addComment} />
			<Comments comments = {comments}/>
		</div>
	return (
		<React.Fragment>
			{thread}
		</React.Fragment>
	);
}

// Thread.PropTypes = {
// 	title: PropTypes.string, 
// 	body: PropTypes.string,
// 	userId: PropTypes.number
// }

export default Thread;