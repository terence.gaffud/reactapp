import React from "react";

const Comments = ({comments}) => {
	const showComments = comments.map( comment => (
		<div>
			{comment.body}
		</div>
	)).reverse();

	return (
		<React.Fragment>
			{showComments}
		</React.Fragment>
	)
}

export default Comments;