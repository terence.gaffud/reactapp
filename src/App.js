import React, { Component, useState, useEffect } from 'react';
import uuid from "uuid";

import Threads from "./components/Threads"; //gets the threads.js
import ThreadForm from "./components/ThreadForm"; //gets the ThreadForm.js

const App = () => {

  const state = {
    threads : [],
    showForm: false,
    isLoading: true
  }

  const [isLoading, setIsLoading] = useState(true);
  const [threads, setThreads] = useState([]);
  const [showForm, setShowForm] = useState(false);


  useEffect(()=> {
    console.log("component did mount")
    fetch("https://jsonplaceholder.typicode.com/posts")
    .then(res => res.json())
    .then(threads => {
      threads.map( thread => {
        thread.comments = []
        return thread;
      })
      setThreads(threads);
      setIsLoading(false);
    })
  }, [])

  const style = {
    "width" : "50%",
    "margin": "0 auto"
  }

  const threadFormSubmit = ({title, body}) => {
    const newThread = {
      "userId":1,
      "id": uuid.v4(),
      "title": title,
      "body": body
    }

    setThreads([...threads, newThread]);
    setShowForm(false);
  }


  const createThreadClickHandler = () => {
    setShowForm(true)
  }

  const threadFormCancel = () => {
    setShowForm(false)
  }

  const deleteThread = (threadId) => {
    let newThreads = threads.filter(thread => thread.id !== threadId );
    setThreads(newThreads);
  }


  const editThread = (threadId, title, body) => {
    let editedThreads = threads.map(thread => {
      if(thread.id === threadId){
        thread.title = title;
        thread.body = body;
        return thread;
      } else {
        return thread;
      }
    });
    setThreads(editedThreads)
  }

  const addComment = (id, commentBody) => {
    const updatedThreads = threads.map(thread => {
      if(thread.id === id){
        let newComment = {
          userId : 1,
          id: uuid.v4(),
          body: commentBody
        }
        thread.comments.push(newComment)
      }
      return thread;
    })
    setThreads(updatedThreads);
  }

    const threadForm = showForm ? 
      <ThreadForm submit= {threadFormSubmit} cancel = {threadFormCancel}/> : 
      <button onClick={createThreadClickHandler}>Create New Thread</button>

    const nothreads = threads.length === 0 ? <h2>No posts</h2> : 
    <Threads threads={threads} deleteThread = {deleteThread} editThread = {editThread} addComment = {addComment}/> 

    const loading = isLoading ? "Loading data" : nothreads

    return (
      <div style = {style} className="App">
        {threadForm}
        {loading}
      </div>
    );
}

export default App;
